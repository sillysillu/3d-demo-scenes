# README #

### What is this repository for? ###
* Version: Demo Demo. :)
* Link to corresponding repository 'release' of [Demo Demo](https://bitbucket.org/sillysillu/3d-demo-scenes/commits/407fa269d4301261a91b0267cd3a152c9c3d4759)
* [More information](https://courses.cs.ut.ee/2016/cg/fall/Main/Project-3DDemoScenes)

### How do I get set up? ###
As we currently do not have a standalone build, you need to:  

* Download Unreal Engine 4.14  
* Download/Clone [repository](https://bitbucket.org/sillysillu/3d-demo-scenes/downloads)  
* Open project from repository with existing project selection in UE  
* Finally, open a level. There are currently 2 levels: Lonely Island and NewMap  (will be renamed as Lobby). + FirstPersonExampleMap, which will be removed soon.  

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact